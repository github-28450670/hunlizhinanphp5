<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/17
 * Time: 19:22
 */

namespace app\ht\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\image;

class Order extends Middle
{
    /*
    *
    * 构造函数，用于导入外部文件和公共方法
    */
    public function _initialize(){
        $this->Order = M('Order');
    }


    public function nofinish(){
        $list=$this->Order->where("del=0 AND status=0")->select();
        foreach ($list as $k => $v) {
            $username=M("user")->where("del=0 AND id=".$v['uid'])->value("name");
            $list[$k]['name']=$username?$username:M("user")->where("del=0 AND id=".$v['uid'])->value("uname");
            $list[$k]['shopname']=M("shangchang")->where("id=".$v["shop_id"])->value("name");
        }
        $this->assign('list',$list);
        return view(); // 输出模板

    }
    public function finish(){
        $list=$this->Order->where("del=0 AND status=1")->select();
        foreach ($list as $k => $v) {
            $username=M("user")->where("del=0 AND id=".$v['uid'])->value("name");
            $list[$k]['name']=$username?$username:M("user")->where("del=0 AND id=".$v['uid'])->value("uname");
            $list[$k]['shopname']=M("shangchang")->where("id=".$v["shop_id"])->value("name");
        }
        $this->assign('list',$list);
        return view(); // 输出模板

    }
    public function order_shenhe(){
        $id=I("post.id");
        if($id){
            $data['status']=1;
            $data['finishtime']=time();
            $re=$this->Order->where("id=$id")->update($data);
            if($re){
                return (1);
            }else{
                return (0);
            }
        }else{
            return (0);
        }
    }
    public function order_finish(){
        $id=I("post.id");
        if($id){
            $data['status']=3;
            $re=$this->Order->where("id=$id")->update($data);
            if($re){
                return (1);
            }else{
                return (0);
            }
        }else{
            return (0);
        }
    }
    public function order_cancel(){
        $id=I("post.id");
        if($id){
            $data['finishtime']="";
            $data['status']=0;
            $re=$this->Order->where("id=$id")->save($data);
            if($re){
                return (1);
            }else{
                return (0);
            }
        }else{
            return (0);
        }
    }
    public function order_return(){
        $id=I("post.id");
        if($id){
            $re=$this->Order->where("id=$id")->delete();
            if($re){
                return (1);
            }else{
                return (0);
            }
        }else{
            return (0);
        }
    }


}