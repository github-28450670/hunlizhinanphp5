<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/17
 * Time: 16:17
 */

namespace app\ht\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\image;

class People extends Middle
{
    public function index()
    {
        $result = M('people')->order('sort desc,id')->select();
//        dump($result);
//        foreach ($result as $k => $v) {
//            $result[$k]['photo'] = array_filter(explode(',', $v['photo']))[1];
//        }
//        dump($result);
//        die;
        $this->assign('result', $result);
        return view();
    }


    public function add()
    {

        if (IS_GET) {
            return view();
        } else {

            $array = array(
                'name' => $_POST['name'],
                'intro' => $_POST['intro'],
                'profession' => $_POST['profession'],
                'sort' => (int)$_POST['sort'],





            );
            $up_arr = array();
            if (!empty($_FILES["file"]["tmp_name"])) {
                $file = request()->file('file');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/'.'UploadFiles/'.'people');
                if($info){

                    $data['photo'] = 'UploadFiles/' .'people/'.$info->getSaveName();

                }else{
                    echo $file->getError();
                }


            }

                $array['photo'] = $data['photo'];
            }
        $db=Db::name('people');
        $up = $db->insert($array);


            if ($up) {
                $this->success("添加成功");
            } else {
                $this->error("添加失败");
            }

        }


    public function del()
    {


        $id = I('get.id', 0, 'intval');;
        if (empty($id)) {
            $this->error('数据错误');
        }
        $up = M('people')->where(['id' => $id])->delete();
        if ($up) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}