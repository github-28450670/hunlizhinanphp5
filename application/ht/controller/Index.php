<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/16
 * Time: 14:35
 */
namespace app\ht\controller;
use think\Request;
class Index extends Middle
{


    public function index(){
        $menu="";
        $index="";
        $menu="<include File='Page/adminusermenu'/>";
        $index="<iframe src='".U('Page/adminindex')."' id='iframe' name='iframe'></iframe>";

        //版权
        $copy=M('web')->where('id=5')->value('concent');
        $this->assign('copy',$copy);
        $this->assign('menu',$menu);
        $this->assign('index',$index);
        return view();
    }
    /**
     * [welcome 首页]
     * @return [type] [description]
     */
    public function welcome(){
        $request = Request::instance();
        $ip=$request->ip();

       // $ip=get_client_ip();
        $todaytime=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
        // 模型
        $order=M('order');
        $user=M('user');
        $shop=M("shangchang");

        //订单数、用户数、交易额、品牌数、产品数 总数
        $ordernum=$order->where("del=0")->count();
        $usernum=$user->where("del=0")->count();
        $shopnum=$shop->count();


        //订单数、用户数、交易额、品牌数、产品数 今日
        $today_usernum=$user->where("del=0 AND addtime>=".$todaytime)->count();
        $today_shopnum=$shop->where("addtime>=".$todaytime)->count();

        //订单数、用户数、交易额、品牌数、产品数 本月 
        $thismonth_usernum=$user->where("del=0 AND addtime>=".$beginThismonth)->count();
        $thismonth_shopnum=$shop->where("addtime>=".$beginThismonth)->count();

        $this->assign("usernum",$usernum);
        $this->assign("ordernum",$ordernum);
        $this->assign("shopnum",$shopnum);

        $this->assign("today_usernum",$today_usernum);
        $this->assign("today_ordernum",$today_ordernum);
        $this->assign("today_shopnum",$today_shopnum);

        $this->assign("thismonth_usernum",$thismonth_usernum);
        $this->assign("thismonth_shopnum",$thismonth_shopnum);
        $this->assign("thismonthorder",$thismonthorder);//本月订单数
        $this->assign("ip",$ip);
        return view();
    }
//        public function index(){
//            return view();
//        }
//    public function indexpage(){
//        $todaytime=mktime(0,0,0,date('m'),date('d'),date('Y'));
//        $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
//        // 模型
//        $order=M('order');
//        $user=M('user');
//        $product=M("product");
//        //订单数、用户数、交易额
//        $ordernum=$order->where("del=0")->count();
//        $usernum=$user->where("del=0")->count();
//        $paylist=$order->where("del=0 AND status>10 AND back!=2")->field("amount")->select();
//
//        $money=0;
//        foreach ($paylist as $k => $v) {
//            $money=$money+$v['amount'];
//        }
//        $today_ordernum=$order->where("del=0 AND addtime>=".$todaytime)->count();
//        $today_usernum=$user->where("del=0 AND addtime>=".$todaytime)->count();
//        $today_paylist=$order->where("del=0 AND status>10 AND back!=2 AND addtime>=".$todaytime)->field("price")->count();
//        $today_money=0;
//        if(is_array($today_paylist))    //add
//        {
//            foreach ($today_paylist as $k => $v) {
//                $today_money = $today_money + $v['price'];
//            }
//        }
//        $productnum=$product->where("del=0")->count();
//        $today_productnum=$product->where("del=0 AND addtime>=".$todaytime);
//
//        //订单绘图
//        $thismonthorder=$order->where("del=0 AND back!=2 AND addtime>=".$beginThismonth)->count();
//        $thismonthpaylist=$order->where("del=0 AND back!=2 AND addtime>=".$beginThismonth)->field("price")->select();
//        $thismonthmoney=0;
////        foreach ($thismonthpay as $k => $v) {
////            $thismonthmoney=$thismonthmoney+$v['price'];
////        }
//
//        $this->assign("usernum",$usernum);
//        $this->assign("money",$money);
//        $this->assign("ordernum",$ordernum);
//        $this->assign("productnum",$productnum);
//        $this->assign("today_usernum",$today_usernum);
//        $this->assign("today_money",$today_money);
//        $this->assign("today_ordernum",$today_ordernum);
//        $this->assign("today_productnum",$today_productnum);
//
//        $this->assign("thismonthmoney",$thismonthmoney);//本月的销售额
//        $this->assign("thismonthorder",$thismonthorder);//本月订单数
//       return view();
//    }
}