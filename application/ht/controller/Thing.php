<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/17
 * Time: 16:52
 */

namespace app\ht\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\image;

class Thing extends Middle
{
    public function index()
    {
        $res = M('thing')->order('sort desc,id')->select();

        $this->assign('res', $res);

        return view();
    }

    public function add()
    {

        if (IS_POST) {

            $arr = '';
            $arr['name'] = I('post.name');
            $id = I('post.pro_id');
            $arr['sort']=I('post.sort');
            $arr['content'] = I('post.content');

            //上传产品小图
            if (strpos($arr['content'], 'width="100%"')) {
                $arr['content'] = str_replace(' width="100%"', '', $arr['content']);
            }
            //为img标签添加一个width
            $arr['content'] = str_replace('alt=""', 'alt="" width="100%"', $arr['content']);

            if (!empty($_FILES["photo_x"]["tmp_name"])) {

                $file = request()->file('photo_x');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/'.'UploadFiles/'.'thing');
                if($info){

                    $arr['photo'] = 'UploadFiles/' .'thing/'.$info->getSaveName();
                    $xt = M('thing')->where('id=' . intval($id))->field('photo')->find();
                    if (intval($id) && $xt['photo']) {
                        $img_url = "__STATUS__/" . $xt['photo'];
                        if (file_exists($img_url)) {
                            @unlink($img_url);
                        }
                    }
                }else{
                    echo $file->getError();
                }



//                //文件上传
//                $info = $this->upload_images($_FILES["photo_x"], array('jpg', 'png', 'jpeg'), "thing/" . date(Ymd));
//                if (!is_array($info)) {// 上传错误提示错误信息
//                    $this->error($info);
//                    exit();
//                } else {// 上传成功 获取上传文件信息
//                    $arr['photo'] = 'UploadFiles/' . $info['savepath'] . $info['savename'];
//                    $xt = M('thing')->where('id=' . intval($id))->field('photo')->find();
//                    if ($id && $xt['photo']) {
//                        $img_url = "Data/" . $xt['photo'];
//                        if (file_exists($img_url)) {
//                            @unlink($img_url);
//                        }
//                    }
//                }
            }

            //上传产品大图
            if (!empty($_FILES["photo_d"]["tmp_name"])) {
                $file = request()->file('photo_d');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/'.'UploadFiles/'.'thing');
                if($info){

                    $arr['photos'] = 'UploadFiles/' .'thing/'.$info->getSaveName();
                    $xt = M('thing')->where('id=' . intval($id))->field('photo')->find();
                    if (intval($id) && $xt['photos']) {
                        $img_url = "__STATUS__/" . $xt['photos'];
                        if (file_exists($img_url)) {
                            @unlink($img_url);
                        }
                    }
                }else{
                    echo $file->getError();
                }



                //文件上传
//                $info = $this->upload_images($_FILES["photo_d"], array('jpg', 'png', 'jpeg'), "thing/" . date(Ymd));
//                if (!is_array($info)) {// 上传错误提示错误信息
//                    $this->error($info);
//                    exit();
//                } else {// 上传成功 获取上传文件信息
//                    $arr['photos'] = 'UploadFiles/' . $info['savepath'] . $info['savename'];
//                    $dt = M('thing')->where('id=' . intval($id))->field('photos')->find();
//                    if ($id && $dt['photos']) {
//                        $img_url2 = "Data/" . $dt['photos'];
//                        if (file_exists($img_url2)) {
//                            @unlink($img_url2);
//                        }
//                    }
//                }
            }

            if (intval($id) > 0) {

                $sql = M('thing')->where('id=' . intval($id))->update($arr);
            } else {
                $arr['addtime'] = time();
                $db=Db::name('thing');
                $sql = $db->insert($arr);

                $id = $sql;
            }

            //规格操作
            if ($sql) {//name="guige_name[]
                $this->success('操作成功.');
                exit();
            } else {
                $this->error('操作失败');
            }


        } else {

            return view();
        }


    }

    public function show(){
        if(IS_GET){
            $id=I('get.id');
            $pro_allinfo=M('Thing')->where(['id'=>$id])->find();
            if($pro_allinfo){
                $this->assign('pro_allinfo',$pro_allinfo);
                return view();
            }else{
                $this->error('没有这个id');
            }

        }
    }

    public function del()
    {
        $id = I('get.did');
        if (empty($id)) {
            $this->error('数据有误');
        }
        $up = M('thing')->where(['id' => $id])->delete();
        if ($up) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }   
}