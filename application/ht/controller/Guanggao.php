<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/17
 * Time: 15:04
 */

namespace app\ht\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\image;

class Guanggao extends Middle
{
    public function _initialize(){
        $this->guanggao = M('guanggao');
    }

    /*
    *
    * 获取、查询广告表数据
    */
    public function index(){

        //搜索，根据广告标题搜索
        $adv_name = I('post.adv_name');
        $condition = array();

        if ($adv_name) {
            $condition['name'] = array('LIKE','%'.$adv_name.'%');
            $this->assign('name',$adv_name);
        }

        $adv_list = $this->guanggao->where($condition)->order('addtime desc')->select();

        $this->assign('adv_list',$adv_list);

       return view(); // 输出模板

    }


    /*
    *
    * 跳转添加或修改广告数据页面
    */
    public function add(){
        //如果是修改，则查询对应广告信息
        if (intval(I('get.adv_id'))) {
            $adv_id = intval(I('get.adv_id'));

            $adv_info = $this->guanggao->where('id='.intval($adv_id))->find();
            if (!$adv_info) {
                $this->error('没有找到相关信息.');
                exit();
            }
            $this->assign('adv_info',$adv_info);
        }
       return view();
    }


    /*
    *
    * 添加或修改广告信息
    */
    public function save(){
        $data['name']=I("post.name");
        //上传广告图片
        if (!empty($_FILES["file"]["tmp_name"])) {
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public/' . 'static/'.'UploadFiles/'.'adv');
            if($info){

                $data['photo'] = 'UploadFiles/' .'adv/'.$info->getSaveName();
                $xt = M('guanggao')->where('id='.intval($_POST['adv_id']))->find();
                if (intval($_POST['adv_id']) && $xt['photo']) {
                    $img_url = "__STATUS__/" . $xt['photo'];
                    if (file_exists($img_url)) {
                        @unlink($img_url);
                    }
                }
            }else{
                echo $file->getError();
            }

        }
        $data['position']=	I('post.position');
        if(empty($data['position'])){
            $this->error('请选择显示位置');
        }
        $data['sort']=I('post.sort');
        //保存数据
        if (intval($_POST['adv_id'])) {

            $result = $this->guanggao->where(['id'=>I('post.adv_id')])->update($data);


        }else{
            //保存添加时间
            $this->guanggao->addtime = time();
            $data['position']=	I('post.position');
            if(empty( $data['position'])){
                $this->error('请选择显示位置');
            }
            if($data['position']==2){
                $re=M('guanggao')->where(['position'=>2])->find();
                if($re){
                    $this->error('已经存在价格表');
                    exit();
                }
            }

                $db=Db::name('guanggao');
                $result = $db->insert($data);


        }
        //判断数据是否更新成功
        if ($result) {
            $this->success('操作成功.','index');
        }else{
            $this->error('操作失败.');
        }
    }

    /*
    *
    * 广告删除
    */
    public function del(){
        //获取广告id，查询数据库是否有这条数据
        $adv_id = intval($_GET['did']);
        $check_info = $this->guanggao->where('id='.intval($adv_id))->find();
        if (!$check_info) {
            $this->error('系统繁忙，请时候再试！');
            exit();
        }

        //修改对应的删除状态
        $up = $this->guanggao->where('id='.intval($adv_id))->delete();
        if ($up) {
            $url = "Data/".$check_info['photo'];
            if (file_exists($url)) {
                @unlink($url);
            }
            $this->success('操作成功.','index');
        }else{
            $this->error('操作失败.');
        }
    }
}