<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/19
 * Time: 10:10
 */

namespace app\ht\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\image;
use think\Request;

class shangchang extends Middle
{

    public function _initialize(){
        $this->category = M('category');
    }

    public function shopadd(){
        $id=I("get.id");
        if(IS_POST){
            $shangchang=M("shangchang");
            $data = input('post.');

            unset($data['uploadfile-1']);
            unset($data['uploadfile-2']);
          //  $shangchang->create();
            //上传产品分类缩略图
            if (!empty($_FILES["logo"]["tmp_name"])) {
                $file = request()->file('logo');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/' . 'UploadFiles/' . 'shop/logo/');
                if ($info) {

                    $data['logo'] = 'UploadFiles/' . 'shop/logo/' . $info->getSaveName();

                } else {
                    echo $file->getError();
                }

            }

//
//                //文件上传
//                $info = $this->upload_images($_FILES["logo"],array('jpg','png','jpeg'),"shop/logo/".date(Ymd));
//                if(!is_array($info)) {// 上传错误提示错误信息
//                    $this->error($info);
//                }else{// 上传成功 获取上传文件信息
//                    $shangchang->logo = 'UploadFiles/'.$info['savepath'].$info['savename'];
//                }
//            }




            if (!empty($_FILES["banner"]["tmp_name"])) {
                $file = request()->file('banner');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/' . 'UploadFiles/' . 'shop/banner/');
                if ($info) {

                    $data['banner'] = 'UploadFiles/' . 'shop/banner/' . $info->getSaveName();

                } else {
                    echo $file->getError();
                }
            }

                //文件上传
//                $info2 = $this->upload_images($_FILES["banner"],array('jpg','png','jpeg'),"shop/banner/".date(Ymd));
//                if(!is_array($info2)) {// 上传错误提示错误信息
//                    $this->error($info2);
//                }else{// 上传成功 获取上传文件信息
//                    $shangchang->banner = 'UploadFiles/'.$info2['savepath'].$info2['savename'];
//                }
//            }
            //组装一个省市区的名字出来
            $post_sheng=M('ChinaCity')->where('id='.(int)$_POST['sheng'])->value('name');
            $post_city =M('ChinaCity')->where('id='.(int)$_POST['city'])->value('name');
            $post_quyu =M('ChinaCity')->where('id='.(int)$_POST['quyu'])->value('name');
            $data['address_xq'] = $post_sheng.' '.$post_city.' '.$post_quyu.' '. $_POST['address'] ;
            if($id>0){
                $re=$shangchang->where("id=$id")->update($data);
            }else{
               $data['addtime']=time();
                $db=Db::name('shangchang');
                $re = $db->insert($data);
               // $re=$shangchang->add();
            }
            if($re){
                $this->success("提交成功!");
            }else{
                $this->error("提交失败!");
            }

        }else{

            if($id){
                $info=M("shangchang")->where("id=$id")->find();
                $this->assign("info",$info);
                $scq=M('shangchang')->where("id=$id")->find();
                $sheng=$scq['sheng'];
                $city=$scq['city'];
                $quyu=$scq['quyu'];
            }
            $output_sheng=$this->city_option($sheng,0,1);
            $output_city=$this->city_option($city,$sheng);
            $output_quyu=$this->city_option($quyu,$city);
            $shopcat=$this->category->where("tid=6")->field("id,name")->select();
            $level=$this->category->where("tid=1")->field("id,name,bz_4")->order("sort asc")->select();
            $this->assign('output_sheng',$output_sheng);
            $this->assign('output_city',$output_city);
            $this->assign('output_quyu',$output_quyu);
            $this->assign("id",$id);
            $this->assign("shopcat",$shopcat);
            $this->assign("level",$level);
            return view();
        }
    }
    public function shoplist(){
        $list=M("shangchang")->select();
        foreach ($list as $k => $v) {
            $list[$k]['levelname']=$this->category->where("id=".$v['level'])->value("name");
            $list[$k]['catname']=$this->category->where("id=".$v['cid'])->value("name");
        }
        $this->assign("list",$list);
        return view();
    }
    public function shop_disable(){
        if(IS_AJAX){
            $id=I("post.id");
            $act=I("post.act");
            if(!$id){
                //$this->ajaxReturn("参数有误!");
                return ('参数有误');
            }
            $re=M("shangchang")->where("id=$id")->setField("del",$act);
            if($re){
                $returnid= $act==0? 1 :0 ;
             //  return($returnid);
               return ($returnid);
            }else{
                return ('修改失败');
              // return("修改失败!");
            }
        }
    }
    public function shop_del(){
        if(IS_AJAX){
            $id=I("post.id");
            if($id){
                $re=M("shangchang")->where("id=$id")->delete();
                if($re){
                   return(1);
                }else{
                   return(0);
                }
            }
        }
    }

    public function shop_photo(){
        $id=I("get.id");
        if(!$id){
            $this->error("未指定店铺!无法查看图片!");exit;
        }
        $list=M("shop_photo")->where("shop_id=$id AND del=0")->order("sort desc,id asc")->select();
        $this->assign("id",$id);
        $this->assign("list",$list);
        return view();
    }

    public function shop_photo_add(){
        $id=I("get.id");
        $this->assign("shopid",$id);
        return view();
    }
    public function shop_photo_del(){
        if(IS_AJAX){
            $id=I("post.id");
            if($id){
                $re=M("shop_photo")->where("id=$id")->delete();
                if($re){
                   return (1);
                }else{
                   return(0);
                }
            }
        }
    }
    public function webuploader(){
        $shopid=I("get.shopid");
        if (!empty($_FILES["file"]["tmp_name"])) {
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public/' . 'static/'.'UploadFiles/'.'shop/photo/');
            if($info) {

                $data['photo'] = 'UploadFiles/' . 'shop/photo/' . $info->getSaveName();
            }




            //文件上传
//            $info = $this->upload_images($_FILES["file"],array('jpg','png','jpeg'),"shop/photo/".date(Ymd));
//            if(!is_array($info)) {// 上传错误提示错误信息
//                $this->error($info);
//            }else{// 上传成功 获取上传文件信息
//                $photo = 'UploadFiles/'.$info['savepath'].$info['savename'];
//            }
            $data["shop_id"]=$shopid;
           // $data["photo"]=$photo;
            $data["addtime"]=time();
            $db=Db::name('shop_photo');
            $sql = $db->insert($data);
           // M("shop_photo")->add($data);
            echo json_encode(array("status"=>1,"err"=>$data['photo']));
        }else{
            echo json_encode(array("status"=>0,"err"=>"上传失败!"));
        }

    }

    public function shopcat(){
        $list=$this->category->where("tid=6")->select();
        $this->assign("list",$list);
        return view();
    }

    public function shopcat_tj(){
        if(IS_AJAX){

            $id=I("post.id");
            $act=I("post.act");
            if(!$id){
                //$this->ajaxReturn("参数有误!");
                return ('参数有误');
            }

            $re=$this->category->where("id=$id")->setField("bz_4",$act);

            if($re){

                $returnid= $act==0? 1 :0 ;
                //$this->ajaxReturn($returnid);
                return $returnid;
            }else{
               //return("修改失败!");
                return ("修改失败");
            }
        }
    }
    public function index(){
        return view();
    }
    public function shopcat_add(){
        $id=I("get.id");
        if(IS_POST){

            $data = input('post.');
            unset($data['uploadfile-1']);

            if (!empty($_FILES["bz_1"]["tmp_name"])) {

                $file = request()->file('bz_1');
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $file->move(ROOT_PATH . 'public/' . 'static/' . 'UploadFiles/' . 'category');
                if ($info) {

                    $data['bz_1'] = 'UploadFiles/' . 'category/' . $info->getSaveName();

                } else {
                    echo $file->getError();
                }
            }
            //保存数据
            if ($id>0) {

                $result = $this->category->where("id=$id")->update($data);
            }else{
                $data['tid']=6;
                $data['addtime'] = time();
                $db=Db::name('category');
                $result = $db->insert($data);
               // $result = $this->category->add();
            }
            //判断数据是否更新成功
            if ($result) {
                $this->success('操作成功');
            }else{
                $this->error('操作失败.');
            }
        }else{
            //如果是修改，则查询对应分类信息
            if ($id>0) {
                $info = $this->category->where("id=$id")->find();
                if (!$info) {
                    $this->error('没有找到相关信息.');
                }
                $this->assign('info',$info);
            }
            $this->assign("id",$id);
            return view();
        }
    }
    public function shopcat_del(){
        if(IS_AJAX){
            $id=I("post.id");
            if($id){
                $re=$this->category->where("id=$id")->delete();
                if($re){
                    return (1);
                  // return(1);
                }else{
                    return (0);
                   //return(0);
                }
            }
        }
    }

    public function shoplevel(){
        $list=$this->category->where("tid=1")->select();
        $this->assign("list",$list);
        return view();
    }

    public function shoplevel_edit(){
        $id=I("get.id");
        if(IS_POST){
            //构建数组
            $this->category->create();
            if ($id>0) {
                $result = $this->category->where("id=$id")->save();
            }
            //判断数据是否更新成功
            if ($result) {
                $this->success('操作成功');
            }else{
                $this->error('操作失败.');
            }
        }else{
            //如果是修改，则查询对应分类信息
            if ($id>0) {
                $info = $this->category->where("id=$id")->find();
                if (!$info) {
                    $this->error('没有找到相关信息.');
                }
                $this->assign('info',$info);
            }
            $this->assign("id",$id);
            return view();
        }
    }



    //**********************************************
    //说明：百度坐标捕获
    //**********************************************
    public function baidumap(){
        return view();
    }


    //***********************************
    // 店铺列表的销售统计，
    // 发送过来的是shop_id
    //**********************************
    public function product_tj(){
        $aaa_pts_qx=1;
        if($_SESSION['admininfo']['qx']!=4){
            $shop_id =(int) M('adminuser')->where('id='.$_SESSION['admininfo']['id'])->value('shop_id');
            if($shop_id==0){
                echo '必须先绑定店铺';
                return;
            }
        }else{
            $shop_id=(int)$_GET['shop_id'];
        }

        $type = $_GET['type'];
        $where="1=1";
        $where.= $shop_id>0 ? ' and shop_id='.$shop_id : '';
        for($i=0;$i<12;$i++){
            //日期
            if($type=='m'){
                $day = strtotime(date('Y-m')) - 86400*30*(11-$i);
                $dayend = $day+86400*30;
                $day_String .= ',"'.date('Y/m',$day).'"';
            }else{
                $day = strtotime(date('Y-m-d')) - 86400*(11-$i);
                $dayend = $day+86400;
                $day_String .= ',"'.date('m/d',$day).'"';
            }
            //dump($dayend);exit;
            //$hyxl=select('id','aaa_pts_order',"1 $where and addtime>$day and addtime<$dayend",'num');
            $hyxl=M('order')->field('id')->where("$where AND addtime>$day AND addtime<$dayend")->count();
            $data1.=',['.$i.','.$hyxl.']';
        }

        $today = strtotime(date('Y-m-d'));

        //$tsql="select * from aaa_pts_order where 1 $where";
        define('rows',20);
        $count=M('order')->where($where)->count();
        $rows=ceil($count/rows);
        $orderlist= M('order')->where($where)->order('id desc')->limit(0,rows)->select();
        foreach ($orderlist as $k => $v) {
            $orderlist[$k]['shangchang']=M('shangchang')->where('id='.$v['shop_id'])->value('name');
            $orderlist[$k]['addtime'] = date("Y-m-d H:i:s",$v['addtime']);
        }
        //根据get过来的shop_id输出商家名字
        $shop_id>0 ? $shangchang=M('shangchang')->where("id=$shop_id")->value('name') : NULL;
        //=========================
        // 将变量输出
        //=========================
        $this->assign('id',$id);
        $this->assign('shop_id',$shop_id);
        $this->assign('day_String',$day_String);
        $this->assign('data1',$data1);
        $this->assign('type',$type);
        $this->assign('orderlist',$orderlist);
        $this->assign('shangchang',$shangchang);
        return view();
    }
}