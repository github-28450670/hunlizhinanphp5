<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/18
 * Time: 14:10
 */

namespace app\api\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use think\Upload;

class Index extends Middle
{
    //***************************
    //  首页数据接口
    //***************************
    public function index()
    {
        $cat = M('category')->where('tid=6')->order('sort desc')->field('id,name,bz_1')->select();
        foreach ($cat as $k => $v) {
            $cat[$k]['bz_1'] = __DATAURL__.$v['bz_1'];
            $cat[$k]['bz_1'] =str_replace('\\','/',  $cat[$k]['bz_1'] );
        }

        $shoplist = M('shangchang')->where('del=0')->order('sort desc,id desc')->field('id,name,logo,is_gift,setmenu,exp,level,is_bao,is_tui,is_nuo,is_luck,gift,fans')->limit(8)->select();
        foreach ($shoplist as $k => $v) {
            $shoplist[$k]['logo'] = __DATAURL__.$v['logo'];
           $shoplist[$k]['logo']=str_replace('\\','/', $shoplist[$k]['logo']);
        }

        echo json_encode(array('cat'=>$cat,'shoplist'=>$shoplist));
        exit();
    }


    public function getlist(){
        $page = intval($_REQUEST['page']);
        $limit = intval($page*8)-8;

        $pro_list = M('product')->where('del=0 AND pro_type=1 AND is_down=0 AND type=1')->order('sort desc,id desc')->field('id,name,photo_x,price_yh,shiyong')->limit($limit.',8')->select();
        foreach ($pro_list as $k => $v) {
            $pro_list[$k]['photo_x'] = __DATAURL__.$v['photo_x'];
            $pro_list[$k]['photo_x'] =str_replace('\\','/',  $pro_list[$k]['photo_x'] );
        }

        echo json_encode(array('prolist'=>$pro_list));
        exit();
    }













}