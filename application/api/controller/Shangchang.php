<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/19
 * Time: 16:17
 */

namespace app\api\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use think\Upload;

class Shangchang extends Middle
{
    public function index(){
        $id=I("post.id");
        $is_bao=I("post.is_bao");
        if(!$id){
            echo json_encode(array("status"=>0,"err"=>"请选择分类!"));exit;
        }

        $cat = M('category')->where('tid=6')->order('sort desc')->field('id,name')->select();
        $condition["del"]=0;
        $condition["cid"]=$id;
        if($is_bao){
            $condition["is_bao"]=$is_bao;
        }

        $shoplist = M('shangchang')->where($condition)->order('sort desc,id desc')->field('id,name,logo,is_gift,setmenu,exp,level,is_bao,is_tui,is_nuo,is_luck,gift,fans')->limit(8)->select();
        foreach ($shoplist as $k => $v) {
            $shoplist[$k]['logo'] = __DATAURL__.$v['logo'];
            $shoplist[$k]['logo'] =str_replace('\\','/',    $shoplist[$k]['logo'] );
        }

        echo json_encode(array('cat'=>$cat,'shoplist'=>$shoplist));
        exit();
    }
    public function detail(){

        $id=I("post.shopid");
        $info = M('shangchang')->where("id=$id")->find();
        if (!$info) {
            echo json_encode(array('status'=>0,'err'=>'没有找到店铺信息.'));
            exit();
        }
        $info['logo']=__DATAURL__.$info['logo'];
        $info['logo'] =str_replace('\\','/', $info['logo']);
        $info['banner']=__DATAURL__.$info['banner'];
        $info['banner']=$this->replace($info['banner']);
        $info['photoarr']=M("shop_photo")->where("shop_id=".$id)->field("photo")->select();
        foreach ($info['photoarr'] as $k => $v) {
            $info['photoarr'][$k]["photo"]=__DATAURL__.$v["photo"];
            $info['photoarr'][$k]["photo"] =str_replace('\\','/',$info['photoarr'][$k]["photo"]);
        }


        $content = str_replace(config('content.dir'), __DATAURL__, $info['content']);
        $info['content']= html_entity_decode($content, ENT_QUOTES ,'utf-8');

        echo json_encode(array('status'=>1,'info'=>$info));
        exit();
    }

}