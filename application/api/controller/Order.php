<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/19
 * Time: 16:13
 */

namespace app\api\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use think\Upload;

class Order extends Middle
{
    public function makeorder(){
        $uid=I("post.uid");
        if(!$uid){
            echo json_encode(array("status"=>0,"err"=>"参数有误！"));exit;
        }
        $data["shop_id"]=I("post.shopid");
        $data["uid"]=$uid;
        $data["username"]=I("post.username");
        $data["tel"]=I("post.tel");
        $data["addtime"]=time();
        if(!$this->check_mobile($data["tel"])){
            echo json_encode(array("status"=>0,"err"=>"请输入正确的手机格式！"));exit;
        }

        $db=Db::name('order');
        $re = $db->insert($data);

       // $re=M("order")->add($data);
        if($re){
            @$checked=M("shangchang_sc")->where("uid=$uid AND shop_id=".I("post.shopid"))->find();
            if(!$checked){
                $fans["uid"]=$uid;
                $fans["shop_id"]=I("post.shopid");
                $fans["status"]=1;
                $db=Db::name('shangchang_sc');
                 $db->insert($fans);

                //M("shangchang_sc")->add($fans);
                M("shangchang")->where("id=".I("post.shopid"))->setInc("fans",1);
            }
            echo json_encode(array("status"=>1,"err"=>"提交成功!"));
        }else{
            echo json_encode(array("status"=>0,"err"=>"提交失败!"));
        }
    }
}